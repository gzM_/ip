'''
Created on 10.05.2016

@author: mr42
'''

import os

def create_dir(path):
    path = os.path.abspath(path)
    if not os.path.isdir(path):
        os.makedirs(path)
