'''
Created on 17.06.2016

@author: mr42
'''
import unittest
from data_structures.q_gram_index import QGramIndex
from data_structures.key_words import KeyWords
import os


class TestQGramIndex(unittest.TestCase):

    def setUp(self):
        self.qind = QGramIndex(3)

    def test_find_matches(self):
        self.qind.add_word("ümweltfreundlich")
        self.qind.add_word("umwelt")
        self.qind.add_word("ökologie")
        self.qind.add_word("öko")
        self.qind.add_word("okologie")
        matches = self.qind.find_matches("umwelt", delta=1)
        self.assertEqual(2, len(matches))
        self.assertEqual(("umwelt", 0), matches[0])
        self.assertEqual(("ümweltfreundlich", 1), matches[1])
        matches = self.qind.find_matches("umwelt", delta=0)
        self.assertEqual(1, len(matches))
        self.assertEqual(("umwelt", 0), matches[0])
        matches = self.qind.find_matches("öko", delta=1)
        self.assertEqual(3, len(matches))
        self.assertEqual(("ökologie", 0), matches[0])
        self.assertEqual(("öko", 0), matches[1])
        self.assertEqual(("okologie", 1), matches[2])
        
#         self.assertEqual(("umwelt",11),
#                     self.qind.find_matches("umveltfreundlich", delta=11)[0])
#         self.assertEqual([("umwelt",3), ("öko", 6)],
#                     self.qind.find_matches("umweltöko", delta=6))
#         self.assertEqual(2,
#                     len(self.qind.find_matches("umweltöko", delta=6)))
#         self.assertEqual(1,
#                     len(self.qind.find_matches("umweltöko", delta=5)))
#         self.assertEqual(0,
#                     len(self.qind.find_matches("umveltöko", delta=3)))
        

    def test_compute_compute_qgrams(self):
        self.assertEqual(["$$u", "$un", "uni"],
                         self.qind._compute_qgrams("uni"))
        self.assertEqual(["$$a"],
                         self.qind._compute_qgrams("a"))

    def test_compute_ped(self):
        self.assertEqual(4, self.qind._compute_ped("uniw", ""))
        self.assertEqual(10, self.qind._compute_ped("", "university"))
        self.assertEqual(1, self.qind._compute_ped("uniw", "university"))
        self.assertEqual(7, self.qind._compute_ped("university", "uniw"))
        self.assertEqual(1, self.qind._compute_ped("öko", "okologie"))
        self.assertEqual(6, self.qind._compute_ped("okologie", "öko"))
        self.assertEqual(0, self.qind._compute_ped("öko", "ökologie"))
        self.assertEqual(5, self.qind._compute_ped("ökologie", "öko"))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()