#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on 09.05.2016

@author: mr42
'''
import unittest
from data_structures.text_collection import TextCollection
import pandas as pd
import os
from pandas import DataFrame
from data_structures.key_words import KeyWords
import math


class TestTextCollection(unittest.TestCase):
    
    def _test_members(self, tc, texts):
        self.assertAlmostEqual(3.4, tc.avdl)
        # test bm25 score computation
        self.assertAlmostEqual(1.4005697095973755,
                tc.inverted_index[u"nachhaltigkeit"][tc[texts[0]]])
        self.assertAlmostEqual(2.3731253159417927,
                tc.inverted_index[u"nachhaltigkeit"][tc[texts[4]]])
        self.assertAlmostEqual(3.5016173689027155,
                tc.inverted_index[u"öko"][tc[texts[3]]])
        self.assertAlmostEqual(1.7010795445403988,
                tc.inverted_index[u"environment"][tc[texts[1]]])
        self.assertAlmostEqual(2.141556368650736,
                tc.inverted_index[u"energiewende"][tc[texts[2]]])
        self.assertAlmostEqual(1.7010795445403988,
                tc.inverted_index[u"sustainibility"][tc[texts[1]]])
        
    def test_constructor(self):
        kw_path = os.path.join(os.path.dirname(os.path.dirname(__file__)),
                               "key_words.xlsx")
        kw = KeyWords(kw_path)
        tx1 = u"Nachhaltigkeit ist schön."
        tx2 = u"Sustainibility is important for our environment."
        tx3 = u"Wir fordern die Energiewende."
        tx4 = u"Öko"
        tx5 = u"Nachhaltigkeit, nachhaltiGKEIT\n NACHhaltigkeit."
        tc = TextCollection([tx1, tx2, tx3, tx4, tx5])
        self._test_members(tc, [tx1, tx2, tx3, tx4, tx5])
        

        # test final result
        res = tc.query(kw)
        self.assertEqual(tx1,res["text"].iloc[4])
        self.assertAlmostEqual(1.4005697095973755, res["score"].iloc[4])
        self.assertEqual(tx2,res["text"].iloc[1])
        self.assertAlmostEqual(3.4021590890807976, res["score"].iloc[1])
        self.assertEqual(tx3,res["text"].iloc[3])
        self.assertAlmostEqual(2.141556368650736, res["score"].iloc[3])
        self.assertEqual(tx4,res["text"].iloc[0])
        self.assertAlmostEqual(3.5016173689027155, res["score"].iloc[0])
        self.assertEqual(tx5,res["text"].iloc[2])
        self.assertAlmostEqual(2.3731253159417927, res["score"].iloc[2])
        
        # self.assertAlmostEqual(2.56380557, tc.total_score)
        
        # change 
        kw_path = os.path.join(os.path.dirname(os.path.dirname(__file__)),
                               "key_words_diff_score.xlsx")
        kw = KeyWords(kw_path)
        tc = TextCollection([tx1, tx2, tx3, tx4, tx5])
        
        self._test_members(tc, [tx1, tx2, tx3, tx4, tx5])
        # test final result
        res = tc.query(kw)
        
        self.assertEqual(tx3,res["text"].iloc[0])
        self.assertAlmostEqual(2.14155636865073*2, res["score"].iloc[0])
        self.assertEqual(tx4,res["text"].iloc[1])
        self.assertAlmostEqual(3.5016173689027155, res["score"].iloc[1])
        self.assertEqual(tx2,res["text"].iloc[2])
        self.assertAlmostEqual(1.7010795445403988*1.5, res["score"].iloc[2])
        self.assertEqual(tx5,res["text"].iloc[3])
        self.assertAlmostEqual(2.3731253159417927 * .5, res["score"].iloc[3])
        self.assertEqual(tx1,res["text"].iloc[4])
        self.assertAlmostEqual(1.4005697095973755 * .5, res["score"].iloc[4])

        # Test save and load functionality
        path = "test_save.csv"
        res.to_csv(path)
        tc = TextCollection.from_csv(path)
        self._test_members(tc, [tx1, tx2, tx3, tx4, tx5])
        os.remove(path)
        kw_path = os.path.join(os.path.dirname(os.path.dirname(__file__)),
                               "key_words_with_intersections.xlsx")
        kw = KeyWords(kw_path)
        tx1 = u"öko" 
        tx2 = u"Unter Nachhaltigkeit verstehen wir bewusst mit der Umwelt umzugehen"
        tx3 = u"Wir sind zertifiziert mit ISO 9001"
        tx4 = u"Wir sind bewusst und mit ISO zertifiziert."
        tx5 = u"Zertifiziert mit ISO nach 5001 Standard."
        ## test intersection
        tc = TextCollection([tx1, tx2, tx3, tx4, tx5])
        self.assertAlmostEqual(3.837798250098801,
                               tc.inverted_index[u"öko"][tc[tx1]])
        self.assertAlmostEqual(1.8379530081118327,
                        tc.inverted_index[u"nachhaltigkeit"][tc[tx2]])
        self.assertAlmostEqual(1.2031242860743754,
                        tc.inverted_index[u"bewusst"][tc[tx4]])
        self.assertAlmostEqual(1.0463897326775897,
                        tc.inverted_index[u"bewusst"][tc[tx2]])
        self.assertAlmostEqual(1.8379530081118327,
                        tc.inverted_index[u"umwelt"][tc[tx2]])
        self.assertAlmostEqual(0.7250332291103154,
                        tc.inverted_index[u"iso"][tc[tx3]])
        self.assertAlmostEqual(0.6707333082425672,
                        tc.inverted_index[u"iso"][tc[tx4]])
        self.assertAlmostEqual(0.7250332291103154,
                        tc.inverted_index[u"iso"][tc[tx5]])
        self.assertAlmostEqual(2.284333268370296,
                        tc.inverted_index[u"9001"][tc[tx3]])
        self.assertAlmostEqual(2.284333268370296,
                        tc.inverted_index[u"5001"][tc[tx5]])

        # test ranking
        res = tc.query(kw)
        self.assertEqual(tx5,res["text"].iloc[0])
        self.assertAlmostEqual(6.743766224, res["score"].iloc[0])
        self.assertEqual(tx2,res["text"].iloc[1])
        self.assertAlmostEqual(4.722295749, res["score"].iloc[1])
        self.assertEqual(tx1,res["text"].iloc[2])
        self.assertAlmostEqual(3.837798250098801, res["score"].iloc[2])
        self.assertEqual(tx3,res["text"].iloc[3])
        self.assertAlmostEqual(3.734399726, res["score"].iloc[3])
        self.assertEqual(tx4,res["text"].iloc[4])
        self.assertAlmostEqual(0.6707333082425672, res["score"].iloc[4])
         
        #test qgram
        kw_path = os.path.join(os.path.dirname(os.path.dirname(__file__)),
                               "key_words_with_intersections.xlsx")
        kw = KeyWords(kw_path)
        tx1 = u"ökologisch" 
        tx2 = u"Im Nachhaltigkeitsbericht beschäftigen wir uns bewusst mit iso."
        tx3 = (u"Im Nachhaltigkeitsbericht beschäftigen wir uns bewusst mit den"
        " Folgen für die Umwelt.")
        tx4 = (u"Nachhaltigkeit steht bei uns ganz oben auf der Liste, denn "
                "wir gehen bewusst mit unserer Umwelt um, "
               "indem wir dem IsO Standard 5001 folgen.")
        tx5 = u"Ecology is important for us. Ökologie ist wichtig für uns."
        tc = TextCollection([tx1, tx2, tx3, tx4, tx5])
        # test tx4
        res = tc.query(kw)
        self.assertEqual(tx4, res["text"].iloc[0])
        self.assertAlmostEqual(tc.inverted_index[u"bewusst"][tc[tx4]] + 
        tc.inverted_index[u"nachhaltigkeit"][tc[tx4]] +
        tc.inverted_index[u"umwelt"][tc[tx4]] +
        tc.inverted_index[u"iso"][tc[tx4]] + 2 *
        (tc.inverted_index[u"iso"][tc[tx4]] +
        tc.inverted_index[u"5001"][tc[tx4]]), res["score"].iloc[0])
        # test tx5
        self.assertEqual(tx5, res["text"].iloc[1])
        self.assertAlmostEqual(tc.inverted_index[u"ökologie"][tc[tx5]] +
        tc.inverted_index[u"ecology"][tc[tx5]], res["score"].iloc[1])
        # test tx1
        self.assertEqual(tx1, res["text"].iloc[2])
        self.assertAlmostEqual(tc.inverted_index[u"ökologisch"][tc[tx1]],
                               res["score"].iloc[2])
        # test tx3
        self.assertEqual(tx3, res["text"].iloc[3])
        self.assertAlmostEqual(tc.inverted_index[u"bewusst"][tc[tx3]] + 
        tc.inverted_index[u"nachhaltigkeitsbericht"][tc[tx3]] +
        tc.inverted_index[u"umwelt"][tc[tx3]], res["score"].iloc[3])
        # test tx2
        self.assertEqual(tx2, res["text"].iloc[4])
        self.assertAlmostEqual(tc.inverted_index[u"iso"][tc[tx2]],
                               res["score"].iloc[4])
        
         # for the documentation
        tx1 = u"Nachhaltig Nachhaltigkeit nachHaltigkeit" 
        tx2 = u"Unter Nachhaltigkeit verstehen wir, bewusst mit der Umwelt umzugehen."
        tx3 = u"Wir sind zertifiziert mit ISO 9001"
        tx4 = u"Wir sind bewusst und mit ISO zertifiziert."
        tx5 = u"Zertifiziert mit ISO nach 5001 Standard."
        ## test intersection
        tc = TextCollection([tx1, tx2, tx3, tx4, tx5])
        kw_path = os.path.join(os.path.dirname(os.path.dirname(__file__)),
                               "key_words_docu.xlsx")
        kw = KeyWords(kw_path)
        print(kw)
        print(kw.intersections)
        res = tc.query(kw)
        print(res["score"].mean())
        print(res)
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()