'''
Created on 20.04.2016

@author: mr42
'''
# imports of other libraries
import os
import sys
from argparse import ArgumentParser
import pandas as pd
import multiprocessing
import requests


def main(args):
    # imports of own libraries must come after the project path is added
    # to the pythonpath
    from log import get_logger   
    from file.path import create_dir
    from data_structures.key_words import KeyWords
    from parallel import ProcessManager
    
    create_dir(args.output)
    log = get_logger(logfile=os.path.join(args.output, "default.log"))
    log.info("Read websites from excel file.")
    websites = pd.read_excel(args.websites, sheetname=None)
    log.info("Read key words from excel file.")
    kw_df = pd.read_excel(args.key_words, sheetname=None)
    kw_dict = {}
    for key, df in kw_df.items():
        kw_dict[key] = KeyWords(df)
    
    log.info("Go through the websites to analyse")
    data = websites["Daten"]
#     websites_per_process = int(len(data) / args.cores) + 1
#     frames = [data.iloc[i*websites_per_process:min((i+1)*websites_per_process,
#               len(data))] for i in range(args.cores)]
    
    # use multiprocessing
    pm = ProcessManager()
    for web_id, web_data in data.iterrows():
        pm.add_task(process_website, args=(args, web_data, kw_dict))
    log.info("Starting %d cores." % (args.cores))
    res = pm.execute(nproc=args.cores)
    log.info("Concatenate the results of all processes.")
    websites["Daten"] = pd.concat(res, axis=1).T
    
    log.info("Save the websites to excel file.")
    writer = pd.ExcelWriter(args.websites, engine='xlsxwriter')
    for sheetname, df in websites.items():
        df.to_excel(writer, sheet_name=sheetname)
    writer.save()
    
def process_website(args, data, kw_dict):
    # imports of own libraries must come after the project path is added
    # to the pythonpath
    from log import get_logger   
    from internet.website import Website
    from internet.browser import Browser
    from data_structures.text_collection import TextCollection
    from data_structures.link_collection import LinkCollection
    from internet.link import Link
    from internet.requester import Requester

    log = get_logger()
    url = data.Internetseite
    requester = Requester()
    if not isinstance(url, float):
        log.info("Process website: %s" % url)
        io_path = url
        if io_path.endswith("/"):
            io_path = io_path[:-1]
        io_path = io_path.replace("https://", "").replace("http://",
                "").replace(".","_").replace("/","_")
        
        csv_path = os.path.join(args.output, io_path + ".csv")
        collections = {"text": None, "link": None}
        csv_paths = {}
        for collection_name in collections.keys():
            csv_paths[collection_name] = os.path.join(args.output,
                                         io_path + "_%s.csv" % collection_name) 
        # check if the csv paths exist. If yes just load the collections
        # from the csv files.
        all_csv_paths_exist = True
        for csv_path in csv_paths.values():
            if not os.path.exists(csv_path):
                all_csv_paths_exist = False

        if all_csv_paths_exist:
            log.info("Load text collection from file.")
            collections["text"] = TextCollection.from_csv(csv_paths["text"])
            log.info("Load media collection from file.")
            collections["link"] = LinkCollection.from_csv(csv_paths["link"])

        else:
            # start new session
            session = requests.Session()
            item = requester.from_url(Link(url, "a", [], add_scheme=True),
                                       lang=args.language, session=session)
            if isinstance(item, Website):
                log.info("Browse the website.")
                browser = Browser(item, kw_dict, args.depth, lang=args.language,
                                  session=session)
                collections["text"] = browser.texts
                collections["link"] = browser.media
                # free memory. especially needed for multiprocessing
                log.info("Delete the browser to free memory.")
                del browser
            log.info("Delete the website to free memory.")
            del item
                #log.info("Build image collection")
                #ic = ImageCollection(tree, kw)
        
        for collection_name, collection in collections.items():
            if collection is not None:
                log.info("Query %s collection with key words." %
                         collection_name) 
                res_list = []
                columns = []
                for key, kw in kw_dict.items():
                    columns.append(key)
                    res = collection.query(kw, score_name=key)
                    # Write bm25 score if the excel sheet name contains bm25
                    # Otherwise only write "ja"/"nein" if a key word is/is not
                    # contained.
                    data_col = key + "-" + collection_name

                    # Write bm25 score if the excel sheet name contains bm25
                    # Otherwise only write "ja"/"nein" if a key word is/is not
                    # contained.
                    if "bm25" not in key:
                        filt = lambda x: "ja" if x > 0 else "nein"
                        res[key] = res[key].map(filt)
                        if "ja" in res[key].values:
                            data.set_value(data_col, "ja")
                        else:
                            data.set_value(data_col, "nein")
                    else:
                        data.set_value(data_col, res[key].mean())
                    res_list.append(res)
                left = res_list.pop()
                for rigth in res_list:
                    if collection_name == "link":
                        left = pd.merge(left, rigth, on=["text", "link"])
                    else:
                        left = pd.merge(left, rigth, on=["text"])
                log.info("Save query result as csv to %s." %

                         csv_paths[collection_name])
                columns.append("text")
                if collection_name == "link":
                    columns.append("link")
                left = left[columns]
                left.to_csv(csv_paths[collection_name], 
                            cols=columns, encoding="utf-8",
                            line_terminator=os.linesep)
                # free memory. especially needed for multiprocessing
        log.info("Delete collection objects to free memory.")
        del collections
    return data


if __name__ == '__main__':
    multiprocessing.freeze_support()
    # Add project directory to PYTHONPATH if does not exist already. This must
    # stand inside of __name__ for running it together with multiprocessing
    code_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    if code_dir not in sys.path:
        sys.path.insert(1, code_dir)
    parser = ArgumentParser()
    parser.add_argument(dest="websites", type=str,
                        help=("The file with the websites to analyse."))
    parser.add_argument(dest="key_words", type=str,
                        help=("The file with the key words of ecology."))
    parser.add_argument("-l", "--lang", dest="language", type=str,
                        default="de",
                        help=("The language code."
                        "Search the given websites for this language."))
    parser.add_argument("-c", "--cores", dest="cores", type=int,
                        default=multiprocessing.cpu_count(),
                        help=("Describes the number of cores used for the "
                        "processing. Per default all available cores are used"))
    parser.add_argument("-d", "--depth", dest="depth", type=int,
                        default=5,
                        help=("Describes the recursion depth limit. "
                              "At this limit the search stops."))
    
    parser.add_argument(dest="output", type=str,
                        help=("The output folder."))
    
    args = parser.parse_args()
    main(args)