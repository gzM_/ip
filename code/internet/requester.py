'''
Created on 30.08.2016

@author: mr42
'''
import requests
from bs4 import BeautifulSoup
from log import get_logger
from internet.link import Link
from internet.website import Website
from urllib.parse import urlparse


class Requester(object):
    '''
    classdocs
    '''
    def __init__(self, website_types=["text/html"]):
        """Create a Requester.
        
        :param website_types: If the MIME type of a requested link match these
                              types it is interpreted as website.
        :type website_types: list
        """
        self.website_types = website_types
        self.log = get_logger()


    def from_url(self, link, lang="de", session=None):
        """Create a Website object if the link refers to a website.
        Otherwise the link is returned. If the link is not
        callable, None is returned.
        
        :param link: The link to request.
        :type link: Link
        :param lang: The language code, e.g. de, en. Crawl links which match
                     this language.
        :type lang: str
        
        :returns: Website or Link or None A Website object is returned,
                  if the MIME type of the given link matches any of website
                  type. Otherwise the link is returned. If the link is not
                  callable, None is returned.

        >>> from log import get_logger
        >>> from internet.website import Website
        >>> from internet.link import Link
        >>> log = get_logger(level="Error")
        >>> req = Requester()
        >>> item = req.from_url(Link("www.badenova.de/", "a", "",
        ...                             add_scheme=True, timeout=0))
        >>> isinstance(item, Website)
        True
        >>> item = req.from_url(Link("bullshit", "a", "", timeout=0))
        >>> item is None
        True
        >>> item = req.from_url(Link("http://rudolph-controlling.de/images/rudolphControlling/Broschuere-Rudolph-Controlling.PDF",
        ...                           "a", "", timeout=0))
        >>> isinstance(item, Link)
        True
           
        """
        
        try:
            headers = {"Accept-Language": "%s;q=1" % lang,
                       'User-agent': 'Mozilla/5.0'}
            if session is None:
                session = requests.Session()
            #headers = {"Accept-Language": "%s;q=1" % lang}
            self.log.info("Try to crawl %s" % str(link))
            r = session.get(link.path, headers=headers, timeout=120)
            # check MIME type     
            mime_type = r.headers.get('content-type')
            # check for error code
            r.raise_for_status()
            if self.is_website(mime_type):
                self.log.info("A website is created")
                item = Website(link, r.text)
            else:
                self.log.info(("The link refers to the media of the MIME type "
                               "%s is returned.") % mime_type)
                item = link
            return item
        except requests.exceptions.Timeout:
            self.log.warn("Timeout when requesting %s." % str(link))
        except:
            self.log.warn("Could not request %s." % str(link))
            return None
    
    def is_website(self, mime_type):
        """Check if the MIME type is a valid website type.

        :param mime_type: The The MIME type to check.
        :type mime_type: str
        
        """
        for wt in self.website_types:
            if wt in mime_type:
                return True

        return False