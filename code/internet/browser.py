#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on 27.04.2016

@author: mr42
'''
from internet.website import Website
from log import get_logger
from internet.requester import Requester
from internet.link import Link
from data_structures.text_collection import TextCollection
from data_structures.link_collection import LinkCollection
import psutil
import os


class Browser(object):
    '''
    classdocs
    '''


    def __init__(self, website, key_words, depth, lang="de", session=None):
        """Create a browser for the given website.
        
        The key words are used to focus the search.
        
        :param root: The website to browse.
        :type root: Website
        :param key_words: Only links containing any key word are
                          followed.
        :type key_words: dict of KeyWords
        :param depth: The recursion depth limit.
        :type depth: int
        :param lang: The language code. Crawl links which match this
                     language.
        :type lang: str
        """
        self.log = get_logger()
        self.root_website = website
        self.visited_links = {}
        self.key_words = key_words
        self.lang = lang
        self.requester = Requester()
        self.session = session
        self.depth = depth
        # build tree
        self._search_texts_and_media()
        
    def _search_texts_and_media(self):
        """Navigate through the website and search for texts and media.
        """
        # init websites with root
        websites = [self.root_website]
        self.media = self.root_website.imgs
        self.texts = self.root_website.texts
        while(len(websites) > 0):
            current = websites.pop()
            if current.level <= self.depth:
                # find the children of current
                links = current.links
                for link in links:
                    # check if the link is a internal one
                    if link.belongs_to_domain(self.root_website.link):
                        # check if link was not called before by checking the url
                        if (link.path not in self.visited_links):
                            self.visited_links.update({link.path: 0})
                            # if link is a image and not a text the link title is
                            #  empty -> check for key words does not work -> follow
                            # link
                            if link.contains_kw(self.key_words) or link.title == "":                        
                                # add website node to queue
                                item = self.requester.from_url(link.join(
                                                           self.root_website.link),
                                                           self.lang,
                                                           self.session)
                                if item is not None:
                                    if isinstance(item, Website):
                                        # extract the texts from the website
                                        # and add it to the queue
                                        item.level = current.level + 1
                                        websites.append(item)
                                        self.texts += item.texts
                                        self.media += item.imgs
                                    elif isinstance(item, Link):
                                        self.media.append(item)
        self.log.info("Build text collection.")
        self.texts = TextCollection(self.texts)
        self.log.info("Build link collection.")
        self.media = LinkCollection(self.media)
        

    def __repr__(self, *args, **kwargs):
        return str(self.nodes)
