'''
Created on 21.04.2016

@author: mr42
'''
import requests
from bs4 import BeautifulSoup
from log import get_logger
from internet.link import Link


class Website(object):
    '''
    classdocs
    '''
    
    def __init__(self, link, html_text):
        """Create a Website object from a given url. 
        
        :param link: The link of a website
        :type link: Link
        :param html_text: The html text defining the website's structure.
        :type html_text: str
        :returns: Website or None A Website object is returned, if everthing
                  works like it should. If the url is not callable or describes
                  a pdf file, None is returned.

        >>> html_text = ("<!DOCTYPE html><html>"
        ...                 "<body><h1>My First Heading</h1>"
        ...                   "<p>My first paragraph.</p>"
        ...                 "</body>"
        ...             "</html>")
        >>> web = Website(Link("www.badenova.de/", "a", [], add_scheme=True),
        ...               html_text)
        >>> web.link
        https://www.badenova.de
                       
        """
        self.link = link
        content = BeautifulSoup(html_text)
        self.texts = self._find_texts(content)
        self.links = self._find_links(content)
        self.imgs = self._find_imgs(content)
        self.level = 0
    
    def __repr__(self, *args, **kwargs):
        return "link: %s, level: %s" % (str(self.link), self.level)
    
    def _find_texts(self, content):
        """Return all p tags.
                       
        :param content: The html source code of the website
        :type content: navigable str
        Returns
        -------
        result : list
                 The list of all texts.
        
        """
        # remove script and style tags to not have source code as texts.
        for script in content(["script", "style"]):
            if script:
                script.extract()
        # use dictionary to avoid duplicates -> saves memory
        texts = {}
        for text in content.stripped_strings:
            if text is not None:
                texts[text] = 0
        return list(texts.keys())
            
    
    def _find_links(self, content):
        """
        Find all links of the website. Meaning, the website is searched
        for a-tags. If the website contains also frames the src attribute of 
        each frame is interpreted as link.
        
        :param content: The html source code of the website
        :type content: navigable str
        :returns: list The list of website's links.
        
        """
        links = []
        atags = content.find_all("a", {"href":True})
        for atag in atags:
            links.append(Link.from_a_tag(atag))
        # add frame's src attribute
        frame_tags = content.find_all("frame", {"src": True})
        for frame_tag in frame_tags:
            links.append(Link.from_frame_tag(frame_tag))
        return links
        
        
    
    def _find_imgs(self, content):
        """Return all images as Link objects, where the path of the link is
        the path to the image file and the content of the link is the title
        of the image.
                       
        :param content: The html source code of the website
        :type content: navigable str
        
        Returns
        -------
        result : list
                 The Link list of all images.
        
        """
        imgs = content.find_all("img", {"src": True})
        links = []
        for img in imgs:
            links.append(Link.from_img_tag(img))
        return links
    
        