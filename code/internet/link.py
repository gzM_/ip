'''
Created on 27.07.2016

@author: mr42
'''
from urllib.parse import urlparse
from urllib.parse import urljoin
import requests
import os


class Link(object):
    '''
    classdocs
    '''

    def __init__(self, path, tag_type, title, add_scheme=False, timeout=120):
        """Create a new Link object.
        :param path: The path of the link. This path is needed to call the 
                     link in a browser.
        :type path: str
        :param tag_type: The tag type of the link.
        :type tag_type: str
        :param title: The title of the link.
        :type title: str
        :param add_scheme If true and the given path does not contains already
                          a valid scheme the https scheme is added to the path.
        :type boolean, False
        
        """
        # First call of urlparse to check if there's a scheme
        parsed_path = urlparse(path)
        if path.endswith("/"):
            path = path[:-1]
        # add scheme if its required and missing
        if add_scheme and parsed_path.scheme == "":
            # find correct scheme
            try:
                r = requests.get("https://" + path, timeout=timeout)
                # check for error code
                r.raise_for_status()
                path = "https://" + path
            except:
                path = "http://" + path
        # call urlparse again, since the netloc is not recognized if the scheme
        # is missing which can be the case for the first call.
        parsed_path = urlparse(path)
        self.netloc = parsed_path.netloc
        # The id is needed to avoid requesting the same url multiple times
        # For the id the parameters and the query string of the url are removed.
        self.query_str = parsed_path.query
        self.path = path
        self.title = title
        self.tag_type = tag_type

    @classmethod
    def from_a_tag(cls, atag):
        """
        Create a Link object given an a-tag. The a-tag's href is taken as
        path.
        :param cls:
        :type cls:
        :param atag: The a tag.
        :type atag: a tag
        """
        title = ""
        for text in atag.stripped_strings:
            if text is not None:
                title += text + " "
        # remove last " "
        title = title[:-1]
        l = cls(atag.get("href"), "a", title)
        return l
    
    @classmethod
    def from_frame_tag(cls, frame_tag):
        """
        Create a Link object given an frame-tag. The frame-tag's src is taken as
        path.
        :param cls:
        :type cls:
        :param frame_tag: The frame tag.
        :type frame_tag: The frame tag.
        """
        title = ""
        for text in frame_tag.stripped_strings:
            if text is not None:
                title += text + " "
        # remove last " "
        title = title[:-1]
        l = cls(frame_tag.get("src"), "frame", title)
        return l
    
    @classmethod
    def from_img_tag(cls, img_tag):
        """
        Create a Link object given an img-tag. The img-tag's src is taken as
        path.
        :param cls:
        :type cls:
        :param img_tag: The img-tag.
        :type img_tag: The img-tag.
        """
        title = img_tag.get("title")
        # if title is not present take the filename of the image as link content
        if title is None or title == "":
            title = os.path.splitext(os.path.basename(img_tag.get("src")))[0]
        l = cls(img_tag.get("src"), "img", title)
        return l
    
    def belongs_to_domain(self, domain):
        """Check if the link is part of the given domain.
        
        :param domain: The domain.
        :type domain: Link
        >>> domain = Link("www.eon.com", "a", [], add_scheme=True)
        >>> l = Link("/en/about-us.html", "a", "About us", timeout=0)
        >>> l.belongs_to_domain(domain)
        True
        >>> domain = Link("https://www.eon.com", "a", [])
        >>> l = Link("/en/about-us.html", "a", "About us", timeout=0)
        >>> l.belongs_to_domain(domain)
        True
        >>> l = Link("https://www.eon.com/en/about-us.html", "a", "About us",
        ...          timeout=0)
        >>> l.belongs_to_domain(domain)
        True
        >>> l = Link("https://twitter.com/EON_SE_en", "a", "twitter",
        ...          timeout=0)
        >>> l.belongs_to_domain(domain)
        False
        """
        if self.netloc == "" or self.netloc == domain.netloc:
            return True
        return False
    
    def join(self, base):
        """Change the path of this link to the joined version of this link and
        the base link.
        
        For joining the paths the method urllib.parse.urljoin is used.
        
        :param base: The base link. 
        :type base: Link
        >>> domain = Link("www.eon.com", "a", "", add_scheme=True, timeout=0)
        >>> l = Link("/en/about-us.html", "a", "About us", timeout=0)
        >>> l = l.join(domain)
        >>> domain = Link("https://www.eon.com", "a", "")
        >>> l = Link("/en/about-us.html", "a", "About us",timeout=0)
        >>> l = l.join(domain)
        >>> l.path == "https://www.eon.com/en/about-us.html"
        True
        >>> l = Link("https://www.eon.de/en/about-us.html", "a", "About us",
        ...          timeout=0)
        >>> l = l.join(domain)
        >>> l.path == "https://www.eon.de/en/about-us.html"
        True
        
        """
        self.path = urljoin(base.path, self.path)
        return self
    
    def contains_kw(self, key_words):
        """
        Check if the link contains a key word.
        :param key_words: The key words to check for.
        :type key_words: KeyWords
        :returns: boolean True if link contains a key word. Otherwise false.
        """
        # a frame contains no meaningful title.
        if self.tag_type=="frame":
            return True
           
        # check if the linkcontains any key word
        for kw in key_words.values():
            if kw.contains_key_word(self.title):
                return True
        return False
    
    def __repr__(self):
        """Print the path of the link.
        >>> print(Link("https://www.eon.com", "a", "",timeout=0))
        https://www.eon.com
        >>> print(Link("www.badenova.de/", "a", "", add_scheme=True, timeout=0))
        https://www.badenova.de
        
        """
        return self.path
        