#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on 04.05.2016

@author: mr42
'''
from pandas import DataFrame
import math
import string
import scipy.sparse
import numpy as np
import pandas as pd
from data_structures.q_gram_index import QGramIndex
from log import get_logger
import os
from internet.website import Website
import csv



class TextCollection(dict):
    '''
    classdocs
    '''


    def __init__(self, content_items):
        """Create a TextCollection.
        
        First a inverted index and a q-gram index is computed.
        
        In the next step bm25 scores are computed.
        
        :param content_items: Either a list of texts or websites.
        :type content_items: list
        
        
        """
        super(TextCollection, self).__init__()
        # initialize all members
        self.inverted_index = {}
        self.inverted_index_intersect = {}
        # parameters for computing the bm25 score.
        self.text_len = {}
        self.avdl = 0
        self.k = 1.75
        self.b = 0.75
        self.text_id = 0
        self.qgram_ind = QGramIndex(3)
        # build a inverted index
        self._build_inv_index(content_items)
        # compute bm25 scores
        self._compute_scores()
  
    def _build_inv_index(self, content_items):
        """A inverted index is build.

        In case of a website we extract the texts and add them afterwards to the
        text collection.
        
        In case of a text we just add it to the text collection.
        
        :param content_items: Either a list of texts or websites.
        :type content_items: list
        
        >>> tc = TextCollection([])
        >>> tc._build_inv_index(["text1w1 text1w2 text1w3", 
        ...                      "text2w1 text2w2 text2w3"])
        >>> tc == {"text1w1 text1w2 text1w3": 0,
        ...              "text2w1 text2w2 text2w3": 1}
        True
        
        """
        for item in content_items:
            # The content item is a website
            if isinstance(item, Website):
                self._add_website(item)
            # The content item is a text
            elif isinstance(item, str):
                self._add_text(item)
        # compute the average document length 
        if len(self.text_len) > 0:
            self.avdl /= float(len(self.text_len))
                
    def _add_website(self, website):
        """Add all strings of the website which are not None to the collection.
        
        :param website: The website to add.
        :type website: Website

        """
        # get p tags
        web_texts = website.texts
        #website.test_frame()
        for web_text in web_texts:
            # get content of p tag
            if web_text is not None:
                # add content of p tag to the collection.
                self._add_text(web_text)
            
    def _add_text(self, text):
        """Add a text to the inverted index and q-gram index if it not exists
        already.
        
        First the text gets an unique id. Secondly it is split into
        words. For each word we perform the following steps: 
        
        1. Store the word together with its occurence in the text in the
           inverted index.
           
        2. If the word has not appeared yet, add the q-grams of the word
           to the q-gram index.
        
        :param text: The text to add.
        :type text: str

        >>> tc = TextCollection([])
        >>> tc._add_text("Öko ist nachhaltig öko")
        >>> tc._add_text("Wir sind nachhaltig")
        >>> tc._add_text("-öko-nachhaltig- nach-\\nhaltig")
        >>> # Test the inverted index
        >>> tc.inverted_index == {"öko": {0:2, 2:1}, "ist": {0:1}, "wir": {1:1},
        ...                       "sind": {1:1}, "nachhaltig": {0:1, 1:1, 2:2}}
        True
        >>> # Test the qgram index
        >>> tc.qgram_ind.inverted_list == {"$$ö": [0], "$ök": [0], "öko": [0],
        ...                  "$$w": [3], "$wi": [3], "wir": [3], "$$i": [1],
        ...                  "$is": [1], "ist": [1], "$$s": [4], "$si": [4],
        ...                  "sin": [4], "ind": [4], "$$n": [2], "$na": [2],
        ...                  "nac": [2], "ach": [2], "chh": [2], "hha": [2],
        ...                  "hal": [2], "alt": [2], "lti": [2], "tig": [2]}
        True
        
        """
        # handle everything as a string
        if not isinstance(text, str):
            text = str(text)
        # avoid text duplicates
        if text not in self and len(text) > 0:
            self[text] = self.text_id
            # handle hyphen concatenating a word over two lines.
            text = text.replace("-%s" % os.linesep, "")
            # split by whitespace and newline and hyphen
            words = text.replace("-", " ").split()
            self.text_len[self.text_id] = 0
            for word in words:
                # check if word is not the empty string
                if len(word) > 0:
                    # remove all punctuation marks like .,
                    for p in string.punctuation:
                        word = word.replace(p,"")
                    
                    # If word only consists of punctuations it is empty
                    if len(word) > 0:
                        # update text length
                        self.text_len[self.text_id] += 1
                        self.avdl += 1
                        word = word.lower()
                        # first occurrence
                        if word not in self.inverted_index:
                            self.inverted_index[word] = {}
                            self.qgram_ind.add_word(word)
                        if self.text_id not in self.inverted_index[word]:
                            self.inverted_index[word][self.text_id] = 1
                            # more than one match
                        else:
                            self.inverted_index[word][self.text_id] += 1
            self.text_id += 1
        
    def _compute_scores(self):
        """Compute the BM25 scores. 
        
        bm25 = tf* · log2 (N / df)
        where tf* = tf · (k + 1) / (k  · (1 – b + b · DL / AVDL) + tf)
        and tf: term frequency(key word occurrence)
        and df: document frequency(#documents containing a key word)
        and N: total number of documents
        and DL: document length
        and AVDL: average document length
        and k, b: weighting parameters of BM25
        
        >>> tx1 = "Nachhaltigkeit ist schön."
        >>> tx2 = "Sustainibility is important for our environment."
        >>> tx3 = "Wir fordern die Energiewende."
        >>> tx4 = "Öko"
        >>> tx5 = "Nachhaltigkeit, nachhaltiGKEIT NACHhaltigkeit."
        >>> tc = TextCollection([])
        >>> tc._build_inv_index([tx1, tx2, tx3, tx4, tx5])
        >>> tc._compute_scores()
        >>> tc.inverted_index == {"nachhaltigkeit":
        ... {tc[tx1]:1.4005697095973755, tc[tx5]:2.3731253159417927},
        ...  "öko": {tc[tx4]: 3.5016173689027155},
        ...  "environment": {tc[tx2]: 1.7010795445403988},
        ...  "energiewende": {tc[tx3]: 2.141556368650736},
        ...  "sustainibility": {tc[tx2]:1.7010795445403988},
        ...     'is': {tc[tx2]: 1.7010795445403988},
        ...     'die': {tc[tx3]: 2.141556368650736},
        ...     'fordern': {tc[tx3]: 2.141556368650736},
        ...     'important': {tc[tx2]: 1.7010795445403988},
        ...     'our': {tc[tx2]: 1.7010795445403988},
        ...     'ist': {tc[tx1]: 2.460059794583211},
        ...     'wir': {tc[tx3]: 2.141556368650736},
        ...     'schön': {tc[tx1]: 2.460059794583211},
        ...     'for': {tc[tx2]: 1.7010795445403988}}
        True
        
        """
        for word in self.inverted_index:
            for text_id in self.inverted_index[word]:
                tf_star = self.inverted_index[word][text_id] * (self.k + 1) / (self.k * 
                            (1 - self.b + self.b * 
                             self.text_len[text_id] / self.avdl) +
                            self.inverted_index[word][text_id])
                score = tf_star * math.log(len(self.text_len) /
                                        float(len(self.inverted_index[word])),
                                        2)
                self.inverted_index[word][text_id] = score               
                
    def _intersect(self, d1, d2):
        """Merge two inverted lists. In our case the inverted list is stored
        in a dictionary.
        
        :param d1: First inverted list.
        :type d1: dict
        :param d2: Second inverted list.
        :type d2: dict
        :returns: dict The intesection of both inverted lists.
        
        >>> tc = TextCollection([])
        >>> res = tc._intersect({1: 3, 3: 8, 4: 1, 6: 1}, {2: 5, 3: 1, 4: 2})
        >>> res == {3:9, 4:3}
        True
        
        """
        intersection = {}
        for elem in d1:
            if elem in d2:
                intersection[elem] = d1[elem] + d2[elem]
        return intersection
      
    def _merge(self, merged, d2, score):
        """Merge two inverted lists where the values of the second inverted
        list d2 are multiplied by score.
        
        :param merged: The first inverted list
        :type merged: dict
        :param d2: The second inverted list whose values are multiplied by
                   score.
        :type d2: dict
        :param score: The score for the values of the d2.
        :type score: float

        >>> tc = TextCollection([])
        >>> res = tc._merge({1: 3, 3: 8, 4: 1, 6: 1}, {2: 5, 3: 1, 4: 2}, 2)
        >>> res == {1: 3, 4: 1, 6: 1, 3:10, 4:5, 2: 10}
        True
        
        """ 
        for elem in d2:
            merged[elem] = merged.get(elem, 0) +  score * d2[elem]
        return merged
        

    def query(self, qw, delta=0, score_name="score"):
        """Query the text collection. Return the texts sorted 
        by the bm25 scores.
        
        The idea is to query the text collection with the given key words.
        These texts which match the key words best should get the highest bm25
        score. In the end we sort the texts by the computed bm25 score.
        
        First the intersections are processed. A intersection is a list
        of key words. Only if a text contains all key words of a intersection
        it is considered as match.
        
        Secondly the single key words are processed. If a text contain 
        a key word it is counted as match.
        
        Each match is scored by the bm25 score.
        
        :param qw: The query words.
        :type qw: KeyWords
        :param delta: Threshold for the prefix edit distance. Only words
                      where the prefix edit distance between the word and
                      any key word is <= delta are considered in the search
                      for matching texts.
        :type delta: int
        :param score_name: Defines the name for the score column of the returned
                           DataFrame.
        :type score_name: str
        :returns: DataFrame The text with the corresponding bm25 score. 
                            The data frame is sorted by the bm25 score.
        
        >>> import os
        >>> from data_structures.key_words import KeyWords
        >>> kw_path = os.path.join(os.path.dirname(os.path.dirname(__file__)),
        ...                       "test/key_words_with_intersections.xlsx")
        >>> kw = KeyWords(kw_path)
        >>> tx1 = "ökologisch" 
        >>> tx2 = "Im Nachhaltigkeitsbericht beschäftigen wir uns bewusst mit iso."
        >>> tx3 = ("Im Nachhaltigkeitsbericht beschäftigen wir uns bewusst mit den"
        ...        " Folgen für die Umwelt.")
        >>> tx4 = ("Nachhaltigkeit steht bei uns ganz oben auf der Liste, denn "
        ...        "wir gehen bewusst mit unserer Umwelt um, "
        ...        "indem wir dem IsO Standard 5001 folgen.")
        >>> tx5 = "Ecology is important for us. Ökologie ist wichtig für uns."
        >>> tx6 = ("Wir isolieren bewusster, da Nachhaltigkeit und Umwelt "
        ...        "wichtig sind")
        >>> tc = TextCollection([tx1, tx2,tx3, tx4, tx5, tx6])
        >>> res = tc.query(kw)
        >>> tx4 == res["text"].iloc[0]
        True
        >>> tx5 == res["text"].iloc[1]
        True
        >>> tx1 == res["text"].iloc[2]
        True
        >>> tx3 == res["text"].iloc[3]
        True
        >>> tx2 == res["text"].iloc[4]
        True
        >>> tx6 == res["text"].iloc[5]
        True
        >>> (tc.inverted_index["bewusst"][tc[tx4]] + 
        ... tc.inverted_index["nachhaltigkeit"][tc[tx4]] +
        ... tc.inverted_index["umwelt"][tc[tx4]] +
        ... tc.inverted_index["iso"][tc[tx4]] + 2 *
        ... (tc.inverted_index["iso"][tc[tx4]] +
        ... tc.inverted_index["5001"][tc[tx4]])) == res["score"].iloc[0]
        True
        >>> (tc.inverted_index["ökologie"][tc[tx5]] +
        ... tc.inverted_index["ecology"][tc[tx5]]) == res["score"].iloc[1]
        True
        >>> tc.inverted_index["ökologisch"][tc[tx1]] == res["score"].iloc[2]
        True
        >>> exp = round(tc.inverted_index["bewusst"][tc[tx3]] + 
        ... tc.inverted_index["nachhaltigkeitsbericht"][tc[tx3]] +
        ... tc.inverted_index["umwelt"][tc[tx3]], 14)
        >>> exp == round(res["score"].iloc[3], 14)
        True
        >>> tc.inverted_index["iso"][tc[tx2]] == res["score"].iloc[4]
        True
        >>> 0 == res["score"].iloc[5]
        True
        
        """
        merged = {}
        intersected = []
        # First process the intersections
        for key, (intersection, distances, score) in qw.intersections.items():
            matches = self.qgram_ind.find_matches(intersection[0], delta,
                                                  distances[0])
            d1 = {}
            for word, ped in matches:
                d1 = self._merge(d1, self.inverted_index.get(word, {}), 1)
            
            for kw_id, kw in enumerate(intersection[1:]):
                matches = self.qgram_ind.find_matches(kw, delta,
                                                      distances[kw_id+1])
                merged_matches = {}
                for word, ped in matches:
                    merged_matches = self._merge(merged_matches,
                                        self.inverted_index.get(word, {}), 1)
                d1 = self._intersect(d1, merged_matches)
            intersected.append((d1, score))
            
        # process the single key words
        for kw, (distance, score) in qw.items():
            matches = self.qgram_ind.find_matches(kw, delta, distance)
            for word, ped in matches:
                merged = self._merge(merged, self.inverted_index.get(word,
                                    {}), score)

        # merge the results of the single key words and the intersections.
        for inv_list, score in intersected:
            merged = self._merge(merged, inv_list, score)
        
        
        # sort texts by their bm25 score.
        texts = []
        scores = []
        for text, text_id in self.items():
            texts.append(text)
            scores.append(merged.get(text_id, 0))
        #self.sort(key=lambda x: x[1], reverse=True)    
        df = DataFrame({"text": texts, score_name: scores})
        return df.sort_values(by=[score_name], ascending=False)
    
    @classmethod
    def from_csv(cls,path):
        """Create a TextCollection from a .csv file.
                
        Parameters
        ----------
        path : string
               Path to the .csv file. The csv file should have a text column
               which contains the texts for the text collection.

        """
        df = pd.read_csv(path, encoding='utf-8', lineterminator=os.linesep[0])
        tc = cls(df["text"].tolist())
        return tc
