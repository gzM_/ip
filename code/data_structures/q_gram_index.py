#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on 17.06.2016

@author: mr42
'''
import sys
import numpy as np


class QGramIndex(object):
    '''
    classdocs
    '''


    def __init__(self, q=3):
        """Create an empty q-gram index.
        
        :param q: The length of one gram.
        :type q: int
        
        >>> q = QGramIndex()
        
        """
        self.inverted_list = {}
        self.words = {}
        self.q = q
        self.word_id = 0
        
    def find_matches(self, key_word, delta, max_dist=sys.maxsize):
        """Find all words where the prefix edit distance between the key word
        and the word is at least delta.
        
        First compute the lower bound. If the lower bound is <= 0 compute
        the prefix edit distance to all words. If the lower bound is >= 0
        we only compute the prefix edit distance to all words which
        have at least |lower bound| q-grams in common with the key word.
        
        All words which have a prefix edit distance smaller than delta are
        counted as match.
        
        In the end all matches are sorted by the prefix edit distance in
        ascending order.
        
        :param key_word: The key word for which we want to find matches.
        :type key_word: str
        :param delta: Threshold for the prefix edit distance. Only matches
                      with a prefix edit distance smaller or equal this
                      threshold are taken.
        :type delta: int
        :param max_dist: The maximum distance which is tolerated between the
                         length of the key word and a match.
        :type max_dist: int
        
        >>> q = QGramIndex()
        >>> q.add_word("ümweltfreundlich")
        >>> q.add_word("umwelt")
        >>> q.add_word("ökologie")
        >>> q.add_word("öko")
        >>> q.add_word("okologie")
        >>> res = q.find_matches("umwelt", delta=1)
        >>> len(res)
        2
        >>> res[0]
        ('umwelt', 0)
        >>> res[1]
        ('ümweltfreundlich', 1)
        >>> res = q.find_matches("umwelt", delta=1, max_dist=0)
        >>> len(res)
        1
        >>> res = q.find_matches("öko", delta=1)
        >>> len(res)
        3
        >>> res[0]
        ('ökologie', 0)
        >>> res[1]
        ('öko', 0)
        >>> res[2]
        ('okologie', 1)
        >>> q.add_word("ökos")
        >>> res = q.find_matches("öko", delta=0, max_dist=1)
        >>> len(res)
        2
        >>> res[0]
        ('öko', 0)
        >>> res[1]
        ('ökos', 0)
        
        """
        inv_lists = []
        lower_bound = len(key_word) - self.q * delta

        # If the lower bound is >= 0
        # we only compute the prefix edit distance to all words which
        # have at least |lower bound| q-grams in common with the key word.
        if lower_bound > 0:
            qgrams = self._compute_qgrams(key_word)
            for qg in qgrams:
                if qg in self.inverted_list:
                    inv_lists.append(self.inverted_list[qg])
            merged = self._merge(inv_lists)
            candidates = []
        
            for key, value in merged.items():
                if value >= lower_bound:
                    candidates.append(key)
        # take all words as candidates
        else:
            candidates = self.words.keys()
        
        # compute prefix edit distance 
        # All words which have a prefix edit distance smaller than delta are
        # counted as match.
        matches = []
        for c in candidates:
            # check if the length of the key word and a candidate does not
            # differ more than the accepted maximum distance.
            if abs(len(key_word) - len(self.words[c])) <= max_dist:
                ped = self._compute_ped(key_word, self.words[c])
                if ped <= delta:
                    matches.append((self.words[c], ped))
        # sort in ascending order by ped
        return sorted(matches, key=lambda x: x[1])   
    

    def _compute_ped(self, prefix, y):
        """Compute the prefix edit distance between prefix and y.
        
        :param prefix: The prefix word.
        :type prefix: str
        :param y: The other word.
        :type y: str
        :returns: int The prefix edit distance.
        
        >>> q = QGramIndex()
        >>> q._compute_ped("uniw", "")
        4
        >>> q._compute_ped("", "university")
        10
        >>> q._compute_ped("uniw", "university")
        1
        >>> q._compute_ped("university", "uniw")
        7
        >>> q._compute_ped("öko", "okologie")
        1
        >>> q._compute_ped("okologie", "öko")
        6
        >>> q._compute_ped("öko", "ökologie")
        0
        >>> q._compute_ped("ökologie", "öko")
        5
 
        """
        
        if len(prefix) == 0:
            return len(y)
        if len(y) == 0:
            return len(prefix)
        len_x = len(prefix)+1
        len_y = len(y) + 1
        a = np.zeros((len_x, len_y), dtype=int)
        # init first row and column
        for i in range(len_x):
            a[i, 0] = i
        for j in range(len_y):
            a[0, j] = j
        # dynamic programming to compute prefix edit distance
        
        for i in range(1, len_x):
            for j in range(1, len_y): 
                if prefix[i-1] == y[j-1]:
                    a[i,j] = a[i-1, j-1]
                else:
                    a[i,j] = min(a[i,j-1] + 1, a[i-1,j] + 1,a[i-1, j-1] + 1)
        return min(a[len_x-1])
                
    
    def _merge(self, inv_lists):
        """Merge multiple inverted lists.
        
        :param inv_lists: A list of inverted lists, e.g. [[1,2,3],[2,3,5]].
        :type inv_lists: list
        :returns: dict A dictionary where the keys are the key word ids and the
                  value is the score.

        >>> q = QGramIndex()
        >>> res = q._merge([[1,2,3],[2,3,5]])
        >>> res == {1:1, 2:2, 3:2, 5:1}
        True
        
        """
        merged_lists = {}
        for inv_li in inv_lists:
            for kw_id in inv_li:
                if kw_id not in merged_lists:
                    merged_lists[kw_id] = 1
                else:
                    merged_lists[kw_id] += 1
        return merged_lists
    
    def add_word(self, word):
        """Compute the q-grams of the given word and add these to QGramIndex.
        
        First the q-grams of word are computed. Afterwards each q-gram is
        stored together with the word id in the QGramIndex.
        
        :param word: The word to add.
        :type word: str
        
        >>> q = QGramIndex()
        >>> q.add_word("uni")
        >>> q.add_word("univere")
        >>> q.inverted_list == {"$$u": [0, 1], "$un": [0,1], "uni": [0,1],
        ...                     "niv": [1], "ive": [1], "ver": [1], "ere": [1]}
        True
        
        """
        if word not in self.words:
            self.words[self.word_id] = word
            qgrams = self._compute_qgrams(word)
            
            for qg in qgrams:
                if qg not in self.inverted_list:
                    self.inverted_list[qg] = []
                self.inverted_list[qg].append(self.word_id)
            self.word_id += 1
                               
    def _compute_qgrams(self, word):
        """Compute the q-grams of word.       
  
        First q-1 dollar symbols are added at the front of the word.
        Then compute the q-grams.
        
        :param word: The word to build the q-grams from.
        :type word: str
        :returns: list The list of the q-grams.

        >>> q = QGramIndex()
        >>> q._compute_qgrams("uni")
        ['$$u', '$un', 'uni']
        >>> q._compute_qgrams("a")
        ['$$a']
        
        """
        pad = "$" * (self.q - 1)
        word_pad = pad + word
        qgrams = []
        for i in range(0, len(word_pad) - self.q + 1):
            qgrams.append(word_pad[i:i + self.q])
        return qgrams