'''
Created on 16.06.2016

@author: mr42
'''
from data_structures.text_collection import TextCollection
from pandas import DataFrame
import pandas as pd
from pandas import Series
import numpy as np
from internet.link import Link
import csv
import os

class LinkCollection(TextCollection):
    '''
    classdocs
    '''


    def __init__(self, content_items):
        """Create a LinkCollection.
        
        The same steps like for the parent class TextCollection are performed.
        As text the title of the links is added.
        
        :param content_items: A list of links.
        :type content_items: list
        
        >>> from internet.link import Link
        >>> l1 = Link("www.eon.de/iso.pdf", "a", "iso", add_scheme=True, 
        ...           timeout=0)
        >>> l2 = Link("www.eon.de/umwelt.pdf", "a", "umwelt", add_scheme=True,
        ...           timeout=0)
        >>> lc = LinkCollection([l1, l2])
        >>> lc == {"iso": 0, "umwelt": 1}
        True
        
        """
        self.links = {}
        super(LinkCollection, self).__init__(content_items)

    
    def _build_inv_index(self, content_items):
        """A inverted index is build.
        
        :param content_items: A list of media.
        :type content_items: list
        
        """
        # add media
        for item in content_items:
            self._add_link(item)
        # compute the average length of the titles of all media
        if len(self.text_len) > 0:
            self.avdl /= float(len(self.text_len))
        
    def _add_link(self,link):
        """Add the link to the collection.
        
        First the title of the link is added as text to the text collection.
        Afterwards the corresponding link is added to the link dictionary.
        
        :param medium: The link to add.
        :type medium: Link
        """
        # handle everything as a string
        link_title = link.title
        if not isinstance(link_title, str):
            link_title = str(link_title)
        self._add_text(link_title)
        # save the corresponding link path. Beware of that there can exist
        # multiple links for the same title
        if link_title not in self.links:
            self.links[link_title] = {}
        self.links[link_title][link.path] = 0
        

    def query(self, qw, delta=0, score_name="score"):
        """Query the link collection. Return the title and link itself sorted
        by the bm25 scores.
        
        First the query routine of the parent class is called. Afterwards the
        links belonging to the titles are added as columns. Finally the result
        is returned as dataframe.
        
        :param qw: The query words.
        :type qw: KeyWords
        :param delta: Threshold for the prefix edit distance. Only words
                      where the prefix edit distance between the word and
                      any key word is <= delta are considered in the search
                      for matching texts.
        :type delta: int
        :param score_name: Defines the name for the score column of the returned
                           DataFrame.
        :type score_name: str

        :returns: DataFrame The text with the corresponding bm25 score. 
                            The data frame is sorted by the bm25 score.
    
        >>> from internet.link import Link
        >>> l1 = Link("www.eon.de/iso.pdf", "a", "iso", add_scheme=True,
        ...           timeout=0)
        >>> l2 = Link("www.eon.de/umwelt.pdf", "a", "umwelt", add_scheme=True,
        ...           timeout=0)
        >>> l3 = Link("www.eon.de/umwelt2.pdf","a", "umwelt", add_scheme=True,
        ...           timeout=0)
        >>> lc = LinkCollection([l1, l2, l3])
        >>> import os
        >>> from data_structures.key_words import KeyWords
        >>> kw_path = os.path.join(os.path.dirname(os.path.dirname(__file__)),
        ...                       "test/key_words_with_intersections.xlsx")
        >>> kw = KeyWords(kw_path)
        >>> res = lc.query(kw)
        >>> res = res.sort_values(by="link")
        >>> "iso" == res["text"].iloc[0]
        True
        >>> "http://www.eon.de/iso.pdf" == res["link"].iloc[0]
        True
        >>> "umwelt" == res["text"].iloc[1]
        True
        >>> "umwelt" == res["text"].iloc[1]
        True
        >>> "http://www.eon.de/umwelt.pdf" == res["link"].iloc[1]
        True
        >>> "umwelt" == res["text"].iloc[2]
        True
        >>> "http://www.eon.de/umwelt2.pdf" == res["link"].iloc[2]
        True
        >>> lc.inverted_index["iso"][lc["iso"]] == res["score"].iloc[0]
        True
        >>> 0 == res["score"].iloc[1]
        True
        >>> 0 == res["score"].iloc[2]
        True
        >>> lc = LinkCollection([])
        >>> res = lc.query(kw)
        >>> len(res) == 0
        True
        
        
        
        """
        result = super(LinkCollection, self).query(qw, delta, score_name)
        # add link information
        items={"text": [], score_name: [], "link": []}
        cols = items.keys()
        for row_id, row in result.iterrows():
            for link in self.links[row["text"]].keys():
                for col in cols:
#                     if col == "link":
#                         items[col].append("=HYPERLINK(\"%s\")" % link)
#                     else:
                    if col == "link":
                        items[col].append(link)
                    else:
                        items[col].append(row[col])
        return DataFrame(items)
        
    @classmethod
    def from_csv(cls,path):
        """Create a LinkCollection from a .csv file.
        
        path : string
               Path to the .csv file. The csv file should have a text column
               which contains the texts describing the links and a column
               link containing the corresponding links.
        :returns: LinkCollection The loaded LinkCollection.
 
        """
        df = pd.read_csv(path, encoding='utf-8', lineterminator=os.linesep[0])
        links = []
        for index, row in df.iterrows():
            # some links referring to images have an empty src which is
            # interpreted as nan when reading the csv
            row = row.fillna("")
            links.append(Link(row["link"], "a", row["text"]))
        lc = cls(links)
        return lc