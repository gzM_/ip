#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on 16.06.2016

@author: mr42
'''
from pandas import DataFrame
import pandas as pd
import sys
    

class KeyWords(dict):
    '''
    '''


    def __init__(self, df, sheet=0, sep_intersections=";"):
        """Read the key words from a excel file.
        
        Each row consists of one key word in different languages and
        the key word's score. E.g. a row could be umwelt,environment,1.
        Sometimes it is needed to check a term for multiple key words.
        In this case the key words are concatenated by using the given
        seperator.
        

        :param df: Either the path to a excel file or
                   a DataFrame containing the key words.
        :type df: str or DataFrame
        :param sheet: The sheet name or number containing the key words. This
                      is needed, since a excel file can contain multiple sheets/
                      tables.
        :type sheet: str or int
        :param sep_intersections: The seperator between multiple key words.
        :type sep_intersections: str
        
    
        >>> import os
        >>> import sys
        >>> kw_path = os.path.join(os.path.dirname(os.path.dirname(__file__)),
        ...                       "test/key_words_with_intersections.xlsx")
        >>> kw = KeyWords(kw_path)
        >>> len(kw)
        3
        >>> md = sys.maxsize
        >>> kw == {'eco': (md,1), 'öko': (md, 1), 'iso': (0,1)}
        True
        >>> kw.intersections == {'iso;9001': (['iso', '9001'], [md, md], 1),
        ...                     'iso;5001': (['iso', '5001'], [0,0], 2),
        ...                     'umwelt;bewusst;nachhaltigkeit':
        ...                     (['umwelt', 'bewusst', 'nachhaltigkeit'],
        ...                      [md, 0, md], 1)}
        True
        >>> from pandas import DataFrame
        >>> import numpy as np
        >>> df = DataFrame({"de":["öko","iso;9001"], "en":["eco",np.NaN],
        ...                 "distance":[np.nan, "0;0"],"score": [1,2]})
        >>> kw = KeyWords(df)
        >>> len(kw)
        2
        >>> kw == {'eco': (md,1), 'öko': (md,1)}
        True
        >>> kw.intersections== {'iso;9001': (['iso', '9001'], [0,0], 2)}
        True

        """

        super(KeyWords, self).__init__()
        # data is the path to a dataframe
        if isinstance(df, str):
            df = pd.read_excel(df, sheetname=sheet)
        df = self._to_lower_case(df)
        self.intersections = {} 
        self.sep_intersections = sep_intersections
        self._build(df)
        
    def _build(self, df):
        """Extract the key words from the given DataFrame.

        :param df: Data frame containing all key words.
        :type df: DataFrame
        
        """
        for col in df[df.columns.difference(["distance", "score"])]:
            for row, term in enumerate(df[col]):
                if not pd.isnull(term):
                    sub_terms = term.split(self.sep_intersections)
                    # intersection
                    if len(sub_terms) > 1:
                        dist = df.loc[row, "distance"]
                        # check if distance is specified
                        if pd.isnull(dist):
                            sub_dists = [sys.maxsize] * len(sub_terms)
                        else:
                            # use maxsize as distance for sub_terms whithout a
                            # specified distance
                            sub_dists = dist.split(self.sep_intersections)
                            for i in range(len(sub_dists)):
                                if sub_dists[i] == "":
                                    sub_dists[i] = sys.maxsize
                                else:
                                    sub_dists[i] = int(sub_dists[i])
                                
                        self.intersections[term] = (sub_terms, sub_dists,
                                              df.loc[row, "score"])
                    # no intersection
                    else:
                        for sub_term in sub_terms:
                            # check if distance is specified 
                            if pd.isnull(df.loc[row, "distance"]):
                                dist = sys.maxsize
                            else:
                                dist = int(df.loc[row, "distance"])
                            self[sub_term] = (dist, df.loc[row,"score"])
        
    
    def _to_lower_case(self, df):
        """Transform all key words to lower case.
        
        :param df: The data frame containing the key words.
        :type df: DataFrame
        :returns: DataFrame The DataFrame where all the key words are
                  lower case.

        >>> import os
        >>> import numpy as np
        >>> kw_path = os.path.join(os.path.dirname(os.path.dirname(__file__)),
        ...                       "test/key_words_with_intersections.xlsx")
        >>> kw = KeyWords(kw_path)
        >>> from pandas import DataFrame
        >>> df = DataFrame({"de": ["KEya", "KeYb"], "distance": np.nan,
        ...                "score":[0,3]})
        >>> df = kw._to_lower_case(df)
        >>> df.loc[0, "de"]
        'keya'
        >>> df.loc[1, "de"]
        'keyb'

    
        """
        f = lambda x: x.lower() if isinstance(x, str) else x
        return df.applymap(f)
    
    
    def contains_key_word(self, term):
        """Check if term contains any key word.
        
        First term is transformed to lower case. Return true
        if the term contains any single key word which does not belong
        to a intersection. In the next step check the intersections. Return
        true if term contains all key words of an intersection. 
         
        
        :param term: The term to check.
        :type term: str
        :returns: boolean True if term contains a single key word or all
                          key words of an intersection.
                          
        >>> import os
        >>> kw_path = os.path.join(os.path.dirname(os.path.dirname(__file__)),
        ...                       "test/key_words_with_intersections.xlsx")
        >>> kw = KeyWords(kw_path)
        >>> kw.contains_key_word("Wir sind ökologisch")
        True
        >>> kw.contains_key_word("Wir sind umweltbewusst und nachhaltigkeit.")
        True
        >>> kw.contains_key_word("Wir sind umweltbewusst und nachhaltig.")
        False
        >>> kw.contains_key_word("Wir sind nachhaltigkeit")
        False
        >>> kw.contains_key_word("IsO Standard")
        True
        
        """
        
        term_lower = term.lower()
        # check single key words
        for kw, (distance, score) in self.items():
            if kw in term_lower:
                return True
        # check intersections
        for key, (intersection, distances, score) in self.intersections.items():
            contains_all_kw = True
            for kw in intersection:
                if kw not in term_lower:
                    contains_all_kw = False
            if contains_all_kw:
                return True
                
        return False
        